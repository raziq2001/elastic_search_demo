const express = require('express');
const bodyParser = require('body-parser');
const XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;



const { response } = require('express');

const app = express();
app.use(express.static(__dirname, { index: 'demo.ejs'}));
app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);

app.listen(4000, () => console.log('server running at 4000....'));


//I have used the below commented get method to run a bulk json data (accounts.json) for demo purpose 
//which is available in (SearchEngine.js) file

// searching on query
/* app.get('/search/:index/:type', async (req, res) => {
  const { phraseSearch } = require('./SearchEngine');
  const data = await phraseSearch(req.params.index, req.params.type, req.query.q);
  res.json(data);
}); */




//This method depends on my customized json file (edit_json)
 
 
 app.get('/main', function(req, res) {

 

  
  
  
  let xhttp= new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if(this.readyState == 4 && this.status == 200){
      let response= JSON.parse(xhttp.responseText);
       console.log(response.hits.hits[2]._source.brand);
  
      let output="";
      let output1="";
      let output2="";
      let output3="";
      
      for(let i=0; i<response.hits.hits.length;i++){
       if(i==0){
        output=response.hits.hits[i]._source.brand;
       }
       else if(i==2){
        output1=response.hits.hits[i]._source.brand;
       }
       else if(i==3){
        output2=response.hits.hits[i]._source.brand;
      }
      else if(i==4){
        output3=response.hits.hits[i]._source.brand;
      }
      
      
       
      }
      
      res.render(__dirname + "/demo.ejs", {name:output,name2:output1,name3:output2,
        name4:output3,  });  
     
    }
  };
  xhttp.open("GET", "http://localhost:9200/demos/phone/_search", true);
  xhttp.send();


});
 

 


